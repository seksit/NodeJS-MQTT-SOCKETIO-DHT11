/**
 * Created by Seksit.Y on 12/22/2016.
 */
"use strict";
const realtimeMqtt = require("./app");
const express = require("express");
const app = express();
app.set("port", process.env.port || 3000);

realtimeMqtt.httpServer(app).listen(app.get("port"), ()=> {
    console.log("Real-Time Web Interface to MQTT running on port: " + app.get("port"));
});