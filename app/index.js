/**
 * Created by Seksit.Y on 12/5/2016.
 */
"use strict";
const config = require("./config");

let httpServer = (app) => {
    const http = require("http").Server(app);
    const io = require("socket.io")(http);
    require("./socket")(io);

    return http;
};

module.exports = {
    httpServer
};