/**
 * Created by Seksit.Y on 12/5/2016.
 */
"use strict";
if (process.env.NODE_ENV === "production"){
    // Production environment variable
    module.exports = {
        mqtt: {
            broker: process.env.mqttBroker || "",
            will: {
                topic: process.env.mqttWillTopic,
                payload: process.env.mqttWillPayload,
                qos: process.env.mqttWillQos,
                retain: process.env.mqttWillRetain
            }
        }
    }
} else {
    // Development environment variable
    module.exports = require("./development.json");
}