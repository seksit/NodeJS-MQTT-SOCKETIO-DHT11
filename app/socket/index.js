/**
 * Created by Seksit.Y on 12/5/2016.
 */
"use strict";
const config = require("../config");
const mqtt = require("mqtt").connect(config.mqtt.broker, { will: config.mqtt.will });

module.exports = (io) => {
    io.of("home-1/living-room").on("connection", (socket) => {
        socket.on("subscribe", (data) => {
            console.log("Subscribe > topic: " + data.topic);
            // MQTT subscribe topic from broker
            mqtt.subscribe(data.topic);
        });

        socket.on("publish", (data) => {
            console.log("Publish > topic: " + data.topic + ", message: " + data.message);
            // MQTT publish to broker
            mqtt.publish(data.topic, data.message, { qos: 1, retain: true });
        });

        // Listen message from MQTT broker
        mqtt.on("message", (topic, message, packet) => {
            switch (topic) {
                case "home-1/living-room/dht11":
                    // Send to WebSocket
                    socket.emit("mqtt-message", { topic: "home-1/living-room/dht11", message: message.toString() });
                    break;
            }
        });
    });
};
