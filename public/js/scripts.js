(function(){
	$(document).ready(()=>{
		let socket = io("http://localhost:3000/home-1/living-room");

		socket.on("connect", () => {
			console.log("Connected to server!!!");
			socket.emit("subscribe", { topic : "home-1/living-room/dht11" });
			socket.emit("publish", { topic : "home-1/living-room/connection", message : "connected" });
		});

		// Listen WebSocket namespace "home-1/living-room/dht11" event get message
		socket.on("mqtt-message", (data) => {
			switch (data.topic) {
				case "home-1/living-room/dht11":
					console.log("Topic: " + data.topic + ", DHT11: " + data.message);
					break;
			}
		});
	});
}());